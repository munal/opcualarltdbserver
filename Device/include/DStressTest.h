
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceHeader.xslt) 
    on 2019-03-29T12:18:24.728+01:00
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */





#ifndef __DStressTest__H__
#define __DStressTest__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DStressTest.h>

#include <ClientSessionFactory.h>
namespace Device
{




  class DStressTest:public Base_DStressTest
  {

  public:
    /* sample constructor */
    explicit DStressTest (const Configuration::StressTest & config,
			  Parent_DStressTest * parent);
    /* sample dtr */
     ~DStressTest ();




    /* delegators for
       cachevariables and sourcevariables */


    /* delegators for methods */

    UaStatus callStress (OpcUa_Int32 testNb, OpcUa_Int32 nbStress);

    UaStatus callDebugFunction(OpcUa_Int32 debugNb);
  private:
    /* Delete copy constructor and assignment operator */
      DStressTest (const DStressTest &);
      DStressTest & operator= (const DStressTest & other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

  public:
      std::string logID;
  private:
      UaStatus stress (int testNb, int nbStress,UaClientSdk::UaSession* session);
      UaStatus stressMTx(int nbStress,UaClientSdk::UaSession* session);
      UaStatus stressLOCx2(int nbStress,UaClientSdk::UaSession* session);
      UaStatus stressADC(int nbStress,UaClientSdk::UaSession* session);
      UaStatus stressADCTest2(int nbStress,UaClientSdk::UaSession* session);
      UaStatus debugOutputFiberId();
      UaStatus debugFunction(int debugNb);

  };





}

#endif // include guard
