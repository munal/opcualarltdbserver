
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceHeader.xslt) 
    on 2019-01-15T14:39:18.263+01:00
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */





#ifndef __DLArBackend__H__
#define __DLArBackend__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>

#include <LtdbCommon.h>
#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DLArBackend.h>


namespace Device
{




  class DLArBackend:public Base_DLArBackend
  {

  public:
    /* sample constructor */
    explicit DLArBackend (const Configuration::LArBackend & config,
			  Parent_DLArBackend * parent);
    /* sample dtr */
     ~DLArBackend ();




    /* delegators for
       cachevariables and sourcevariables */


    /* delegators for methods */


  private:
    /* Delete copy constructor and assignment operator */
      DLArBackend (const DLArBackend &);
      DLArBackend & operator= (const DLArBackend & other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

  public:
      /*!
	   *  \brief Creation of the ID of a fiber
	   *
	   *  Create an FiberId for identify a fiber, based on the mapping
	   *
	   *  \param ltdbNum   : number of the ltdb
	   *  \param scaNum    : id of the SCA
	   *  \param locx2Num  : id of the locx2
	   *  \param adcNum    : id of the adc
	   *  \return FiberId* : the created FiberId*
	   */
      FiberId* createFiberId(int ltdbNum,int scaNum, int locx2Num , int adcNum);
      /*!
	   *  \brief Creation of the ID of an ADC
	   *
	   *  Create an FiberId for identify an ADC , based on the mapping
	   *
	   *  \param ltdbNum  : number of the ltdb
	   *  \param scaNum   : id of the SCA
	   *  \param locx2Num : id of the locx2
	   *  \param adcNum   : id of the adc
	   *  \return AdcId*  : The created AdcId*
	   */
      AdcId* createAdcId(int ltdbNum,int scaNum, int locx2Num , int adcNum);
  private:
      int mapping[40];
      int mappingSCUserChannel[320];
      int mappingSCUserPos[320];

  };





}

#endif // include guard
