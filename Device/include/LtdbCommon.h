/*
 * LtdbCommon.h
 *
 *  Created on: 27 nov. 2018
 *      Author: efortin
 */

#ifndef DEVICE_INCLUDE_LTDBCOMMON_H_
#define DEVICE_INCLUDE_LTDBCOMMON_H_

#define CPPM true

#define METHOD_IN_PROGRESS 1
#define METHOD_GOOD 2
#define METHOD_BAD 3

std::string tostr (int x);
std::string tostrdec (int x);

class FiberId{
	public:

		FiberId(int latomeIdInit,int fiberIdInit,std::string connectionFileInit,std::string latomeDeviceInit);
		int getLatomeId(){return latomeId;}
		int getFiberId(){return fiberId;}
		std::string getLatomeDevice(){return latomeDevice;}
		std::string getConnectionFile(){return connectionFile;}
		void logPrint();

	protected:

		int latomeId;
		int fiberId; //Number of fiber LTDB side
		std::string connectionFile;
		std::string latomeDevice;
};

class AdcId : public FiberId{
public:
	~AdcId(){delete[] channelPosUser;delete[] userStreamId;delete[] channelPosIstage;}
	AdcId(int latomeIdInit,int fiberIdInit,std::string connectionFileInit,std::string latomeDeviceInit,int *channelPosUserInit,int *userStreamIdInit,int *channelPosIstageInit,int istageStreamIdInit);
	int getChannelPosUser(int channel){return channelPosUser[channel-1];}
	int getUserStreamId(int channel){return userStreamId[channel-1];}
	int getChannelPosIstage(int channel){return channelPosIstage[channel-1];}
	int getIstageStreamId(){return istageStreamId;}
	void logPrint();
protected:
	int *channelPosUser; //size 4
	int *userStreamId;
	int *channelPosIstage;
	int istageStreamId;
};

#endif /* DEVICE_INCLUDE_LTDBCOMMON_H_ */
