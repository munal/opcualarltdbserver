

#include <LogIt.h>
#include <DigitalIO.h>
#include <sstream>
#include <string>
#include <iomanip>
#include <LtdbCommon.h>

std::string tostr (int x)
{
	std::stringstream str;
	str <<std::hex<< x;
	return str.str();
}
std::string tostrdec (int x)
{
	std::stringstream str;
	str << x;
	return str.str();
}

FiberId::FiberId(int latomeIdInit,int fiberIdInit,std::string connectionFileInit,std::string latomeDeviceInit){
	latomeId =latomeIdInit;
	fiberId = fiberIdInit;
	connectionFile=connectionFileInit;
	latomeDevice=latomeDeviceInit;
}
AdcId::AdcId(int latomeIdInit,int fiberIdInit,std::string connectionFileInit,std::string latomeDeviceInit,
		int *channelPosUserInit,int *userStreamIdInit,int *channelPosIstageInit,int istageStreamIdInit)
						:FiberId(latomeIdInit,fiberIdInit,connectionFileInit,latomeDeviceInit){
	channelPosUser=channelPosUserInit;
	userStreamId=userStreamIdInit;
	channelPosIstage=channelPosIstageInit;
	istageStreamId=istageStreamIdInit;
}
void AdcId::logPrint(){
	  FiberId::logPrint();
	  LOG(Log::INF)<<"istageStreamId"<<istageStreamId;
	  for (int i=0;i<4;i++){
		  LOG(Log::INF)<<"Channel:"<<i+1;
		  LOG(Log::INF)<<"channelPosUser"<<channelPosUser[i];
		  LOG(Log::INF)<<"userStreamId"<<userStreamId[i];
		  LOG(Log::INF)<<"channelPosIstage"<<channelPosIstage[i];
	  }
}
void FiberId::logPrint(){
	  LOG(Log::INF)<<"latomeId"<<latomeId;
	  LOG(Log::INF)<<"fiberId"<<fiberId;
	  LOG(Log::INF)<<"connectionFile"<<connectionFile;
	  LOG(Log::INF)<<"latomeDevice"<<latomeDevice;
}
