
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceBody.xslt) 
    on 2018-11-27T13:23:42.388+01:00
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */




#include <Configuration.hxx>

#include <DLtdbFiberPos2.h>
#include <ASLtdbFiberPos2.h>

#include <LogIt.h>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>
#include <LtdbCommon.h>


namespace Device
{




  // 1111111111111111111111111111111111111111111111111111111111111111111111111
  // 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
  // 1     Users don't modify this code!!!!                                  1
  // 1     If you modify this code you may start a fire or a flood somewhere,1
  // 1     and some human being may possible cease to exist. You don't want  1
  // 1     to be charged with that!                                          1 
  // 1111111111111111111111111111111111111111111111111111111111111111111111111






  // 2222222222222222222222222222222222222222222222222222222222222222222222222
  // 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
  // 2     (code for which only stubs were generated automatically)          2
  // 2     You should add the implementation but dont alter the headers      2
  // 2     (apart from constructor, in which you should complete initializati2
  // 2     on list)                                                          2 
  // 2222222222222222222222222222222222222222222222222222222222222222222222222

  /* sample ctr */
  DLtdbFiberPos2::DLtdbFiberPos2 (const Configuration::LtdbFiberPos2 & config,
				  Parent_DLtdbFiberPos2 *
				  parent):Base_DLtdbFiberPos2 (config, parent)
    /* fill up constructor initialization list here */
  {
    /* fill up constructor body here */
	  //FiberId* DLArBackend::createFiberId(int ltdbNum,int scaNum, int locx2Num , int adcNum)
	  fiberId=getParent()->getParent()->getParent()->getParent()->createFiberId(
	  std::stoi(getParent()->getParent()->getParent()->id()),
	  std::stoi(getParent()->getParent()->id()),
	  std::stoi(getParent()->id()),
	  3);
	  totalFiberNum=(std::stoi(getParent()->id())-1)*2+(std::stoi(getParent()->getParent()->id())-1)*8+2;
  }

  /* sample dtr */
  DLtdbFiberPos2::~DLtdbFiberPos2 ()

  {
	  delete(fiberId);
  }

  /* delegators for cachevariables and externalvariables */

  UaStatus DLtdbFiberPos2::writeCallSetDelay (const OpcUa_Boolean & v)
  {
	  LOG(Log::INF)<<"Called setDelay from register for fiber 2";
		getAddressSpaceLink()->setSetDelayError(METHOD_IN_PROGRESS,OpcUa_Good);
		UaStatus status = callSetDelay(getAddressSpaceLink()->getSetDelayDelay() & 255 , getAddressSpaceLink()->getSetDelayClk () & 255);
		if (!status.isGood()){
			getAddressSpaceLink()->setSetDelayError(METHOD_BAD,OpcUa_Good);
		}
		else {
			getAddressSpaceLink()->setSetDelayError(METHOD_GOOD,OpcUa_Good);
		}
		return status;
  }
  UaStatus DLtdbFiberPos2::callSetDelay (OpcUa_Byte delay ,   OpcUa_Byte clkphase)
  {
	  OpcUa_Byte offset = 0;
	  OpcUa_Byte adcType = 0;

	  OpcUa_Byte readingMask0a=63;
	  UaByteString bs;
	  OpcUa_Byte oneByte;
	  OpcUa_Byte delayC =delay;
	  OpcUa_Byte delayD = delay+offset;
	//  LOG(Log::INF)<<"setDelay(delay ="<<delay<<"  clkphase="<<clkphase<<") fiber "<<totalFiberNum;
	  try{
		 // LOG(Log::INF)<<"DLtdbFiberPos2::callSetDelay with delay " << (int)delay << " and clkphase :" << (int)clkphase;

		  std::unique_ptr<UaClientSdk::UaSession> session(ClientSessionFactory::connect(getParent()->getParent()->getParent()->scaOpcUaEndpoints().c_str()));
		  if (!session){
			  return OpcUa_BadCommunicationError;
		  }
		  std::string baseNodeId="scaFelix"+getParent()->getParent()->id()+".BusI2C_" + tostr(getParent()->getBusNumber());
		  std::string nodeId3 =baseNodeId +".register0" + tostr(getParent()->getBaseAddress()+2);
		  UaoClientForOpcUaSca::I2cSlave register3(session.get(),UaNodeId(nodeId3.c_str(),2));

		  std::string nodeId4 =baseNodeId+".register0" + tostr(getParent()->getBaseAddress()+3);
		  UaoClientForOpcUaSca::I2cSlave register4(session.get(),UaNodeId(nodeId4.c_str(),2));



		  //Register 0a
		  UaByteString readBs(register3.readValue());


		  if (readBs.length()>0){
			  //LOG(Log::INF)<<"Data read:" << (int)*(readBs.data());
			  oneByte=(*(readBs.data()) & readingMask0a ) + ((delayC % 4 ) << 6) ;
			  bs.setByteString(sizeof oneByte,&oneByte);
			  register3.writeValue(bs);
		  }
		  else {
			  LOG(Log::ERR)<<"Error reading the register" << nodeId3;

			  return OpcUa_Bad;
		  }


		  //Register 0b
		  oneByte = (delayD << 3) + ( delayC >> 2 ) ;
		  bs.setByteString(sizeof oneByte,&oneByte);
		  register4.writeValue(bs);



	  }
	  catch(const std::exception& e){
		  LOG(Log::ERR)<<"Caught:"<<e.what();
		  return OpcUa_Bad;
	  }
	  return OpcUa_Good;
  }

  UaStatus DLtdbFiberPos2::writeCallReadDelay (const OpcUa_Boolean & v)
  {
	OpcUa_Int32 delayC,delayD;
	getAddressSpaceLink()->setReadDelayError(METHOD_IN_PROGRESS,OpcUa_Good);


	UaStatus status = callReadDelay(delayC,delayD);
	getAddressSpaceLink()->setReadDelayDelayC(delayC,OpcUa_Good);
	getAddressSpaceLink()->setReadDelayDelayD(delayD,OpcUa_Good);
	if (!status.isGood()){
		getAddressSpaceLink()->setReadDelayError(METHOD_BAD,OpcUa_Good);
	}
	else {
		getAddressSpaceLink()->setReadDelayError(METHOD_GOOD,OpcUa_Good);
	}
	return status;
  }
  UaStatus DLtdbFiberPos2::callReadDelay (OpcUa_Int32 & delayC, OpcUa_Int32 & delayD)
  {
	  LOG(Log::INF)<<"readDelay fiber "<<totalFiberNum;
	  std::unique_ptr<UaClientSdk::UaSession> session(ClientSessionFactory::connect(getParent()->getParent()->getParent()->scaOpcUaEndpoints().c_str()));
	  	  if (!session.get()){
	  		  return OpcUa_BadCommunicationError;
	  	  }
		  std::string baseNodeId="scaFelix"+getParent()->getParent()->id()+".BusI2C_" + tostr(getParent()->getBusNumber());

		  std::string nodeId3 =baseNodeId +".register0" + tostr(getParent()->getBaseAddress()+2);
		  UaoClientForOpcUaSca::I2cSlave register3(session.get(),UaNodeId(nodeId3.c_str(),2));

		  std::string nodeId4 =baseNodeId+".register0" + tostr(getParent()->getBaseAddress()+3);
		  UaoClientForOpcUaSca::I2cSlave register4(session.get(),UaNodeId(nodeId4.c_str(),2));

	  	  UaByteString read4(register4.readValue());
	  	  if (read4.length()!=1){
	  		  LOG(Log::ERR)<<"Error reading register"+ nodeId4;

	  		  return OpcUa_Bad;
	  	  }
	  	  UaByteString read3(register3.readValue());
	  	  	  if (read3.length()!=1){
	  	  		  LOG(Log::ERR)<<"Error reading register"+ nodeId3;

	  	  		  return OpcUa_Bad;
	  	  	  }
	  	  delayC=((read3.data()[0] & 192)>>6) + ((read4.data()[0]&7)<<2);
	  	  delayD=(read4.data()[0] & 248)>>3;
		 // LOG(Log::INF)<< "delay C:0x"<<std::hex<< delayC <<"("<<delayC<<")";
		 // LOG(Log::INF)<< "delay D:0x"<<std::hex<< delayD <<"("<<delayD<<")";

	  	  return OpcUa_Good;
  }
  // 3333333333333333333333333333333333333333333333333333333333333333333333333
  // 3     FULLY CUSTOM CODE STARTS HERE                                     3
  // 3     Below you put bodies for custom methods defined for this class.   3
  // 3     You can do whatever you want, but please be decent.               3
  // 3333333333333333333333333333333333333333333333333333333333333333333333333


  FiberId* DLtdbFiberPos2::getFiberId(){
	  return fiberId;
  }
  void DLtdbFiberPos2::reload_FiberId(){
      	delete fiberId;
      	fiberId=getParent()->getParent()->getParent()->getParent()->createFiberId(
      				  std::stoi(getParent()->getParent()->getParent()->id()),
      				  std::stoi(getParent()->getParent()->id()),
      				  std::stoi(getParent()->id()),
      				  3);
      }
}
