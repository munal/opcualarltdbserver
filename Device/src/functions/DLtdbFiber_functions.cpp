
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceBody.xslt) 
    on 2019-03-25T16:48:56.939+01:00
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */




#include <Configuration.hxx>

#include <DLtdbFiber.h>
#include <ASLtdbFiber.h>

#include <LogIt.h>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>
#include <LtdbCommon.h>
#include <DMon.h>
#include <DFiberMon.h>
#include <ASFiberMon.h>
#include <string>



namespace Device
{




  // 1111111111111111111111111111111111111111111111111111111111111111111111111
  // 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
  // 1     Users don't modify this code!!!!                                  1
  // 1     If you modify this code you may start a fire or a flood somewhere,1
  // 1     and some human being may possible cease to exist. You don't want  1
  // 1     to be charged with that!                                          1 
  // 1111111111111111111111111111111111111111111111111111111111111111111111111






  // 2222222222222222222222222222222222222222222222222222222222222222222222222
  // 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
  // 2     (code for which only stubs were generated automatically)          2
  // 2     You should add the implementation but dont alter the headers      2
  // 2     (apart from constructor, in which you should complete initializati2
  // 2     on list)                                                          2 
  // 2222222222222222222222222222222222222222222222222222222222222222222222222


  /* delegators for cachevariables and externalvariables */

  UaStatus DLtdbFiber::setDelay (OpcUa_Byte delay, OpcUa_Byte clkphase,UaClientSdk::UaSession* session)
  {
	  OpcUa_Byte offset = 0;
	  OpcUa_Byte adcType = 0;

	  OpcUa_Byte readingMask0a=63;
	  UaByteString bs;
	  OpcUa_Byte oneByte;
	  OpcUa_Byte delay1 =delay;
	  OpcUa_Byte delay2 = delay+offset;
	//  LOG(Log::INF)<<"setDelay(delay ="<<delay<<"  clkphase="<<clkphase<<") fiber "<<totalFiberNum;
	  try{
		 // LOG(Log::INF)<<"DLtdbFiberPos2::callSetDelay with delay " << (int)delay << " and clkphase :" << (int)clkphase;

		 /* std::unique_ptr<UaClientSdk::UaSession> session(ClientSessionFactory::connect(getParent()->getParent()->getParent()->scaOpcUaEndpoints().c_str()));
		  if (!session){
			  return OpcUa_BadCommunicationError;
		  }*/
		  std::string baseNodeId="scaFelix"+getParent()->getParent()->id()+".BusI2C_" + tostr(getParent()->getBusNumber());
		  std::string nodeId1 =baseNodeId+".register0" + tostr(getParent()->getBaseAddress());
		  UaoClientForOpcUaSca::I2cSlave register1(session,UaNodeId(nodeId1.c_str(),2));

		  std::string nodeId2 =baseNodeId+".register0" + tostr(getParent()->getBaseAddress()+1);
		  UaoClientForOpcUaSca::I2cSlave register2(session,UaNodeId(nodeId2.c_str(),2));

		  std::string nodeId3 =baseNodeId +".register0" + tostr(getParent()->getBaseAddress()+2);
		  UaoClientForOpcUaSca::I2cSlave register3(session,UaNodeId(nodeId3.c_str(),2));

		  std::string nodeId4 =baseNodeId+".register0" + tostr(getParent()->getBaseAddress()+3);
		  UaoClientForOpcUaSca::I2cSlave register4(session,UaNodeId(nodeId4.c_str(),2));

		  if(fiberPos()==1){
			  readingMask0a=192;
			  //Register 08
			  oneByte=0x14;
			  bs.setByteString(sizeof oneByte,&oneByte);
			  register1.writeValue(bs);

			  //Register 09
			  //LOG(Log::INF)<<"delayA input :" <<std::hex << (int) delayA;
			//  LOG(Log::INF)<<"delayA%16:" <<std::hex << (int) (delayA%16);
			//  LOG(Log::INF)<<"delayA%16<<4:" <<std::hex << (int) ((delayA%16)<<4);
			  oneByte=(((delay1 % 16) << 4) + ((clkphase & 3 )<<2) + (adcType & 3));
			 // LOG(Log::INF)<<"Data write 09:" <<std::hex << (int) oneByte;
			  bs.setByteString(sizeof oneByte,&oneByte);
			  register2.writeValue(bs);

			  //Register 0a
			 // LOG(Log::INF)<<"Reading";
			  UaByteString readBs(register3.readValue());
			  if (readBs.length()>0){
				 // LOG(Log::INF)<<"Data read:" << (int)*(readBs.data());
				  oneByte=(*(readBs.data()) & readingMask0a ) + (delay2 <<1) + (delay1>>4);
				 // LOG(Log::INF)<<"Data write 0a:" <<std::hex << (int) oneByte;
				  bs.setByteString(sizeof oneByte,&oneByte);
				  register3.writeValue(bs);
			  }
			  else {
				  LOG(Log::ERR) << logID <<"Error reading the register0a";
				  return OpcUa_Bad;
			  }
		  }
		  else if(fiberPos()==2){
			  readingMask0a=63;
			  //Register 0a
			  UaByteString readBs(register3.readValue());


			  if (readBs.length()>0){
				  //LOG(Log::INF)<<"Data read:" << (int)*(readBs.data());
				  oneByte=(*(readBs.data()) & readingMask0a ) + ((delay1 % 4 ) << 6) ;
				  bs.setByteString(sizeof oneByte,&oneByte);
				  register3.writeValue(bs);
			  }
			  else {
				  LOG(Log::ERR) << logID <<"Error reading the register" << nodeId3;

				  return OpcUa_Bad;
			  }


			  //Register 0b
			  oneByte = (delay2 << 3) + ( delay1 >> 2 ) ;
			  bs.setByteString(sizeof oneByte,&oneByte);
			  register4.writeValue(bs);

		  }
		  else {
			  LOG(Log::ERR) << logID <<"Wrong value for the fiber Position (defined in config.xml)";
			  return OpcUa_Bad;
		  }

	  }
	  catch(const std::exception& e){
		  LOG(Log::ERR) << logID <<"Caught:"<<e.what();
		  return OpcUa_Bad;
	  }
	  return OpcUa_Good;
  }

  UaStatus DLtdbFiber::readDelay (OpcUa_Int32 & delay1,
				      OpcUa_Int32 & delay2,UaClientSdk::UaSession* session)
  {
	  try{
		 /* std::unique_ptr<UaClientSdk::UaSession> session(ClientSessionFactory::connect(getParent()->getParent()->getParent()->scaOpcUaEndpoints().c_str()));
		  if (!session){
			  return OpcUa_BadCommunicationError;
		  }*/
		  std::string baseNodeId="scaFelix"+getParent()->getParent()->id()+".BusI2C_" + tostr(getParent()->getBusNumber());
		  std::string nodeId1 =baseNodeId+".register0" + tostr(getParent()->getBaseAddress());
		  UaoClientForOpcUaSca::I2cSlave register1(session,UaNodeId(nodeId1.c_str(),2));

		  std::string nodeId2 =baseNodeId+".register0" + tostr(getParent()->getBaseAddress()+1);
		  UaoClientForOpcUaSca::I2cSlave register2(session,UaNodeId(nodeId2.c_str(),2));

		  std::string nodeId3 =baseNodeId +".register0" + tostr(getParent()->getBaseAddress()+2);
		  UaoClientForOpcUaSca::I2cSlave register3(session,UaNodeId(nodeId3.c_str(),2));

		  std::string nodeId4 =baseNodeId+".register0" + tostr(getParent()->getBaseAddress()+3);
		  UaoClientForOpcUaSca::I2cSlave register4(session,UaNodeId(nodeId4.c_str(),2));

		  if (fiberPos()==1){
			  UaByteString read2(register2.readValue());
			  //LOG(Log::INF)<< "read09.data():"<<(int)*read2.data();
			  if (read2.length()!=1){
				  LOG(Log::ERR) << logID <<"Error reading register"+ getParent()->id()+".MTxLOCx2_" + getParent()->id() +".register09";
				  return OpcUa_Bad;
			  }
			  UaByteString read3(register3.readValue());
			  //LOG(Log::INF)<< "read0a.data():"<<(int)*read3.data();
				  if (read3.length()!=1){
					  LOG(Log::ERR) << logID <<"Error reading register"+ getParent()->id()+".MTxLOCx2_" + getParent()->id() +".register0a";

					  return OpcUa_Bad;
				  }

			  delay1=(read2.data()[0] >>4) + ((read3.data()[0]&1)<<4);
			  delay2=(read3.data()[0] & 62)>>1;
		  }
		  else if (fiberPos()==2){
			  UaByteString read4(register4.readValue());
				  if (read4.length()!=1){
					  LOG(Log::ERR) << logID <<"Error reading register"+ nodeId4;

					  return OpcUa_Bad;
				  }
				  UaByteString read3(register3.readValue());
					  if (read3.length()!=1){
						  LOG(Log::ERR) << logID <<"Error reading register"+ nodeId3;

						  return OpcUa_Bad;
					  }
				  delay1=((read3.data()[0] & 192)>>6) + ((read4.data()[0]&7)<<2);
				  delay2=(read4.data()[0] & 248)>>3;
		  }
		  else {
			  LOG(Log::ERR) << logID <<"Wrong value for the fiber Position (defined in config.xml)";
			  return OpcUa_Bad;
		  }
	  }
		 catch(const std::exception& e){
	  		  LOG(Log::ERR) << logID <<"Caught:"<<e.what();
	  		  return OpcUa_Bad;
	  	  }
		 return OpcUa_Good;
  }


  // 3333333333333333333333333333333333333333333333333333333333333333333333333
  // 3     FULLY CUSTOM CODE STARTS HERE                                     3
  // 3     Below you put bodies for custom methods defined for this class.   3
  // 3     You can do whatever you want, but please be decent.               3
  // 3333333333333333333333333333333333333333333333333333333333333333333333333

  FiberId* DLtdbFiber::getFiberId(){
    	  return fiberId;
      }
  void DLtdbFiber::reload_FiberId(){
	delete fiberId;
	fiberId=getParent()->getParent()->getParent()->getParent()->createFiberId(
				  std::stoi(getParent()->getParent()->getParent()->id()),
				  std::stoi(getParent()->getParent()->id()),
				  std::stoi(getParent()->id()),
				  fiberPos()+1);
	getAddressSpaceLink()->setLatome_Fiber(fiberId->getFiberId(),OpcUa_Good);
	for (DFiberMon *fiberMon :getParent()->getParent()->getParent()->mon()->fibermons()){
		if(std::stoi(fiberMon->id())== totalFiberNum){
			fiberMon->getAddressSpaceLink()->setLatomeFiberUsed(fiberId->getFiberId(),OpcUa_Good);
		}
	}
  }




}
