
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
        
    The stub of this file was generated by Quasar (additional info: using transform designToDeviceBody.xslt) 
    on 2018-11-27T13:19:49.422+01:00
    
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
 */




#include <Configuration.hxx>

#include <DLtdb.h>
#include <ASLtdb.h>

#include <LogIt.h>
#include <I2cSlave.h>
#include <ClientSessionFactory.h>
#include <DLocx2Scan.h>
#include <DLtdbSca.h>
#include <DLtdbLOCx2.h>
#include <DLtdbFiber.h>
#include <DStatusLatome.h>
#include <LtdbCommon.h>
#include <DLtdbADCNevis.h>
#include "uhal/uhal.hpp"
#include <boost/thread/thread.hpp>
using namespace uhal;
#include <fstream>
#include <iostream>
#include <LtdbCommon.h>
#include <string>

namespace Device
{


  UaStatus DLtdb::configure (UaClientSdk::UaSession* session)
  {
	  UaStatus s = OpcUa_Bad;
	  LOG(Log::INF) << logID << "Configure";
	  for (DLtdbSca* sca : ltdbscas()){
		  s = sca->configure(session);
		  if (!s.isGood()){return s;}
	  }

	return s;
  }
  UaStatus DLtdb:: corrCoeffADCFromeFiles(UaClientSdk::UaSession* session){
	  for (DLtdbSca* sca : ltdbscas()){
		  for (DLtdbLOCx2* locx2 : sca->ltdblocx2s()){
			  for (DLtdbADCNevis *adcNevis : locx2->ltdbadcneviss()){
				  LOG(Log::INF) << logID << " Sca_"<<sca->id()<<" Adclocx2_"<<locx2->id()<<" AdcNevis_"<<adcNevis->id()<<":"<<"ConfigurationADC : Begin";
				  Log::setNonComponentLogLevel(Log::WRN);
				  UaStatus s=adcNevis->valueFromCalibFiles(session);
				  Log::setNonComponentLogLevel(Log::INF);
				  if (!s.isGood()){return s;}

				  LOG(Log::INF) << logID << " Sca_"<<sca->id()<<" Adclocx2_"<<locx2->id()<<" AdcNevis_"<<adcNevis->id()<<":"<<"ConfigurationADC : Success";
			  }
		  }
	  }
	  return OpcUa_Good;
  }
  UaStatus DLtdb::configureADC (UaClientSdk::UaSession* session)
    {
  	  LOG(Log::INF) << logID << "Configure ADC";
  	  for (DLtdbSca* sca : ltdbscas()){
  		  UaStatus s = sca->configureADC(session);
  		  if (!s.isGood()){return s;}
  	  }
  	return OpcUa_Good;
    }

  UaStatus DLtdb::selectAdcModeAllAdc (OpcUa_Byte numMode,UaClientSdk::UaSession* session){
  	  UaStatus s = OpcUa_Bad;
  	LOG(Log::INF) << logID << "selectAdcModeAllAdc";
  	for (DLtdbSca* sca : ltdbscas()){
  		  s = sca->selectAdcModeAllAdc (numMode,session);
  		  if (!s.isGood()){
  			  return s;
  		  }

  	  }
  	  return OpcUa_Good;
    }

  UaStatus DLtdb::setDelayAllFibers (OpcUa_Byte delay, OpcUa_Byte clkphase,UaClientSdk::UaSession* session){
	  LOG(Log::INF) << logID << "ConfigurationADC";
	   	  for (DLtdbSca* sca : ltdbscas()){
	   		  UaStatus s = sca->setDelayAllFibers(delay,clkphase,session);
	   		  if (!s.isGood()){return s;}
	   	  }
	   	return OpcUa_Good;
  }
  // 3333333333333333333333333333333333333333333333333333333333333333333333333
  // 3     FULLY CUSTOM CODE STARTS HERE                                     3
  // 3     Below you put bodies for custom methods defined for this class.   3
  // 3     You can do whatever you want, but please be decent.               3
  // 3333333333333333333333333333333333333333333333333333333333333333333333333




}
