echo "make_distro: trying to create a package for commit id $1"
DIR=OpcUaLarLtdbServer-$1
rm -Rf $DIR $DIR".tar.gz" 
mkdir $DIR 
cp build/bin/OpcUaLarLtdbServer bin/ServerConfig.xml configGenerator/config_P1_ltdb091.xml configGenerator/config_CPPM_ltdb64.xml configGenerator/config_EMF_ltdb_1c1.xml build/Configuration/Configuration.xsd Design/diagram.pdf Documentation/ConfigDocumentation.html Documentation/AddressSpaceDoc.html $DIR || exit 1 
tar cf $DIR".tar" $DIR 
gzip -9 $DIR".tar"  
