#include <string>
#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include <tabPrint.h>
#include <gen.h>
namespace po = boost::program_options;
using namespace std;

TabPrint TAB(0);

ostream &operator<<(std::ostream &out, const TabPrint &obj){
    obj.print(out);
    return out;
}
ostream &operator<<(std::ostream &out, const AdcGen &obj){
    obj.print(out);
    return out;
}
ostream &operator<<(std::ostream &out, const FiberGen &obj){
    obj.print(out);
    return out;
}
ostream &operator<<(std::ostream &out, const Locx2Gen &obj){
    obj.print(out);
    return out;
}
ostream &operator<<(std::ostream &out, const ScaGen &obj){
    obj.print(out);
    return out;
}
ostream &operator<<(std::ostream &out, const LtdbGen &obj){
    obj.print(out);
    return out;
}
ostream &operator<<(std::ostream &out, const MainGen &obj){
    obj.print(out);
    return out;
}
ostream &operator<<(std::ostream &out, const MonGen &obj){
    obj.print(out);
    return out;
}
int main (int argc, const char *argv[]) {
	po::options_description desc("Allowed options");
	desc.add_options()
    		("help", "produce help message")
    		("card_version", po::value<int>()->default_value(2), "set version of ltdb (1 or 2)")
    		("opcUa_endpoints", po::value<string>()->default_value("localhost:8080"), "set endPoints for felix core")
    		("xsd", po::value<string>()->default_value("../Configuration/Configuration.xsd"), "set path to the xsd file")
    		("connectionLatomeFile", po::value<string>()->default_value("file://connection_latome.xml"), "set path to connection file for latome")
    		("latomeDevice", po::value<string>()->default_value("sc.udp.0"), "set the latome device name")
    		("calibPath", po::value<string>()->default_value("./calib_golden"), "set the calibration file path")
    		("outputFile", po::value<string>()->default_value("config.xml"), "set the output file")
		
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);    

	if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
	}
/*
	if (vm.count("card_version")) {
	    cout << "card_version was set to " 
		 << vm["card_version"].as<int>() << ".\n";
	*/
  MainGen mainGen(
    1,
    vm["card_version"].as<int>(),
    vm["opcUa_endpoints"].as<string>(),
    vm["calibPath"].as<string>(),
    vm["xsd"].as<string>(),
    vm["connectionLatomeFile"].as<string>(),
    vm["latomeDevice"].as<string>()
  );
  std::ofstream configFile(vm["outputFile"].as<string>(), std::ios::out | std::ios::trunc);	
  //cout << mainGen;
  configFile << mainGen;
  configFile.close();	
 }

void TabPrint::print(ostream &out) const{
  for (int i=0;i< tabNb ;i++){
    out << "  ";
  }
}

TabPrint TabPrint::operator++(int){
  TabPrint T=TabPrint(tabNb);
  tabNb++;
  return T;
}
TabPrint TabPrint::operator--(int){
  TabPrint T=TabPrint(tabNb);
  tabNb--;
  return T;
}

void AdcGen::print(ostream &out) const{
  for (int i=1;i<= 4 ;i++){
    out<< TAB << "<LtdbADCNevis name=\"ADC_NEVIS_"<<i<<"\" id=\""<< i<<"\"></LtdbADCNevis>"<<std::endl;
  }
}
void FiberGen::print(ostream &out) const{
  for (int i=1;i<= 2 ;i++){
    out<< TAB << "<LtdbFiber name=\"Fiber_"<<i<<"\" fiberPos=\""<< i<<"\"></LtdbFiber>"<<std::endl;
  }
}
void MonGen::print(ostream &out) const{
  out << TAB << "<Mon name=\"mon\" >"<<std::endl;
  int SCNb=320,fiberNb=40;
  if (ltdbVersion==1){
    SCNb=64;
    fiberNb=8;
  }
  TAB++;
  for (int i=1;i<= SCNb ;i++){
    out<< TAB << "<SCMon name=\"sc_" << i << "\" id=\""<< i <<"\"></SCMon>"<<std::endl;
  }
  for (int i=1;i<= fiberNb ;i++){
    out<< TAB << "<FiberMon name=\"fiber_" << i <<"\" id=\"" << i <<"\"></FiberMon>"<<std::endl;
  }
  TAB--;
   out << TAB << "</Mon>"<<std::endl;
}
void Locx2Gen::print(ostream &out) const{
  AdcGen adcGen;
  FiberGen fiberGen;
    for (int i=1;i<= 4 ;i++){
      out<< TAB << "<LtdbLOCx2 name=\"LOCx2_"<<i<<"\" id=\""<< i<<"\">"<<std::endl;
      TAB++;
      out << fiberGen;
      out << adcGen;
      TAB--;
      out << TAB << "</LtdbLOCx2>"<<std::endl;
    }
}
void ScaGen::print(ostream &out) const{
  int nbSca;
  Locx2Gen locx2Gen;
  if(ltdbVersion == 1){
    nbSca=1;
  }
  else {
    nbSca=5;
  }
  for (int i=1;i<= nbSca ;i++){
    out<< TAB << "<LtdbSca name=\"Sca_"<<i<<"\" id=\""<< i<<"\">"<<std::endl;
    TAB++;
    out << TAB << "<LtdbGpio name=\"gpio\"></LtdbGpio>"<<std::endl;
    out << TAB << "<LtdbMtrx name=\"Mtrx\"></LtdbMtrx>"<<std::endl;
    if (ltdbVersion == 2){
      out << TAB << "<LtdbGBTx name=\"GBTx\"></LtdbGBTx>"<<std::endl;
    }
    for (int i=1;i<=4;i++){
      out << TAB <<"<LtdbMTx name=\"MTx_"<< i <<"\" id=\""<< i <<"\"></LtdbMTx>"<<std::endl;
    }
    out << locx2Gen;
    TAB--;
    out << TAB <<"</LtdbSca>"<<std::endl;
  }
}
void LtdbGen::print(ostream &out) const{
    ScaGen scaGen(ltdbVersion);
    MonGen monGen(ltdbVersion);
    for (int i = 1 ; i<= ltdbNb ; i++){
      out << TAB << "<Ltdb name=\"ltdb_"<< i <<"\""<<std::endl;
      TAB++;
      out << TAB << "id=\""<< i <<"\"" << std::endl;
      out << TAB << "scaOpcUaEndpoints=\"" << scaOpcUaEndpoints<< "\"" << std::endl;
      out << TAB << "ltdbVersion=\"" << ltdbVersion << "\""<< std::endl;
      out << TAB << "calibFilesPath=\"" << calibFilesPath <<"\">"<< std::endl;
      out << TAB << "<StatusLatome name=\"StatusLatome\" id=\"1\"></StatusLatome>" << std::endl;
      out << TAB << "<ClkDesScan name=\"ClkDesScan\"></ClkDesScan>" << std::endl;
      out << TAB << "<Locx2Scan name=\"Locx2Scan\"></Locx2Scan>" << std::endl;
      out << TAB << "<Mapping name=\"Mapping\"></Mapping>" << std::endl;
      out << TAB << "<StressTest name=\"stressTest\"></StressTest>" << std::endl;
      out << scaGen;
      out << monGen;
      TAB--;
      out << TAB << "</Ltdb>"<<std::endl;
    }
}
void MainGen::print(ostream &out) const {
  LtdbGen ltdbGen(ltdbNb,ltdbVersion,scaOpcUaEndpoints,calibFilesPath);
  out << TAB << "<configuration xmlns=\"http://cern.ch/quasar/Configuration\"" << std::endl;
  TAB++;
  out << TAB << "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" << std::endl;
  out << TAB << "xsi:schemaLocation=\"http://cern.ch/quasar/Configuration " << xsdPath << "\">" << std::endl;
  out << TAB << "<LArBackend name=\"larbackend\" connectionLatomeFile=\"" << connectionLatomeFile << "\""<< std::endl;
  out << TAB << "latomeDevice=\"" << latomeDevice << "\" >" << std::endl;
  TAB++;
  out << ltdbGen;
  TAB--;

  out << TAB << "</LArBackend>" << std::endl;
  TAB--;
  out << TAB << "</configuration>" << std::endl;

}
