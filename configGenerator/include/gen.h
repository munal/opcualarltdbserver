#include <iostream>
#include <fstream>
#include <string>
#ifndef _GEN
#define _GEN
using namespace std;
class MonGen {
  public:
    MonGen(int ltdbVersion_i){ltdbVersion=ltdbVersion_i;};
    void print(std::ostream &out) const;
  private:
    int ltdbVersion;
};
class AdcGen {
  public:
    AdcGen(){};
    void print(std::ostream &out) const;
  private:
};
class FiberGen {
  public:
    FiberGen(){};
    void print(std::ostream &out) const;
  private:
};
class Locx2Gen {
  public:
    Locx2Gen(){};
    void print(std::ostream &out) const;
  private:
};
class ScaGen {
  public:
    ScaGen(int ltdbVersion_i){
    ltdbVersion=ltdbVersion_i;};
    void print(std::ostream &out) const;
  private:
    int ltdbVersion;
};
class LtdbGen {
  public:
    LtdbGen(int ltdbNb_i, int ltdbVersion_i , string scaOpcUaEndpoints_i , string calibFilesPath_i){
      ltdbNb=ltdbNb_i;
      ltdbVersion=ltdbVersion_i;
      scaOpcUaEndpoints=scaOpcUaEndpoints_i;
      calibFilesPath=calibFilesPath_i;
    };
    void print(std::ostream &out) const;
  private:
    int ltdbNb;
    int ltdbVersion;
    string scaOpcUaEndpoints;
    string calibFilesPath;
};
class MainGen {
  public:
    MainGen(int ltdbNb_i, int ltdbVersion_i , string scaOpcUaEndpoints_i , string calibFilesPath_i, string xsdPath_i , string connectionLatomeFile_i, string latomeDevice_i){
      ltdbNb=ltdbNb_i;
      ltdbVersion=ltdbVersion_i;
      scaOpcUaEndpoints=scaOpcUaEndpoints_i;
      calibFilesPath=calibFilesPath_i;
      xsdPath = xsdPath_i;
      latomeDevice = latomeDevice_i;
      connectionLatomeFile = connectionLatomeFile_i;
    };
    void print(std::ostream &out) const;
  private:
    int ltdbNb;
    int ltdbVersion;
    string scaOpcUaEndpoints;
    string calibFilesPath;
    string xsdPath;
    string latomeDevice;
    string connectionLatomeFile;
};
#endif
