#include <iostream>
#include <fstream>

#ifndef _TABPRINT
#define _TABPRINT

class TabPrint {
  public:
    TabPrint();
    TabPrint(int tabNbInit) {
      tabNb=tabNbInit;
    }
    void print(std::ostream &out) const;
    TabPrint operator++( int);
    TabPrint operator--( int);
  private:
    int tabNb=0;
};

#endif
