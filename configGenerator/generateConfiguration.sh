#!/bin/sh

echo Generating LAr configuration files...
cd configGenerator
make build
./generate_CPPM
./generate_EMF
./generate_P1
cd ../
echo done
