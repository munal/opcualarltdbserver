source /cvmfs/sft.cern.ch/lcg/contrib/gcc/4.8.4/x86_64-centos7/setup.sh

export BOOST_ROOT=/cvmfs/sft.cern.ch/lcg/releases/Boost/1.59.0_python2.7-682d6/x86_64-centos7-gcc49-opt/
export BOOST_HEADERS=$BOOST_ROOT/include
export BOOST_PATH_HEADERS=$BOOST_ROOT/include
export BOOST_LIB_DIRECTORIES=$BOOST_ROOT/lib
export BOOST_PATH_LIBS=$BOOST_ROOT/lib
export BOOST_LIBS="-lboost_regex -lboost_chrono -lboost_program_options -lboost_thread -lboost_system -lboost_filesystem"

#export LD_LIBRARY_PATH=/opt/cactus/lib:$LD_LIBRARY_PATH
#export PATH=/opt/cactus/bin:$PATH
export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.11.1/Linux-x86_64/bin:$PATH
